#include "sqlite3.h"
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

using std::cout;
using std::endl;
using std::string;
using std::unordered_map;
using std::vector;
using std::pair;

unordered_map<string, vector<string>> results;

int callback(void* notUsed, int argc, char** argv, char** azCol);
int main()
{
	int connect;
	sqlite3* db;
	char* error;
	//create a new data base and connect it to the program
	connect = sqlite3_open("FirstPart.db", &db);
	if (connect)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return 1;
	}
	//create a new table with id, name collumns
	connect = sqlite3_exec(db, "create table people(id integer primary key autoincrement, name text);", callback, 0, &error);
	if (connect != SQLITE_OK)
	{
		cout << "SQL error: " << error << endl;
		sqlite3_free(error);
		system("Pause");
		return 1;
	}
	connect = sqlite3_exec(db, "insert into people(name) values('moti')", NULL, 0, &error);
	if (connect != SQLITE_OK)
	{
		cout << "SQL error: " << error << endl;
		sqlite3_free(error);
		system("Pause");
		return 1;
	}
	connect = sqlite3_exec(db, "insert into people(name) values('shaked')", NULL, 0, &error);
	if (connect != SQLITE_OK)
	{
		cout << "SQL error: " << error << endl;
		sqlite3_free(error);
		system("Pause");
		return 1;
	}
	connect = sqlite3_exec(db, "insert into people(name) values('tahel')", NULL, 0, &error);
	if (connect != SQLITE_OK)
	{
		cout << "SQL error: " << error << endl;
		sqlite3_free(error);
		system("Pause");
		return 1;
	}
	connect = sqlite3_exec(db, "update people set name = 'aminadav' where id = 3", NULL, 0, &error);
	if (connect != SQLITE_OK)
	{
		cout << "SQL error: " << error << endl;
		sqlite3_free(error);
		system("Pause");
		return 1;
	}
	system("PAUSE");
	return 0;
}
/*
this function return the output from sqlite 3
*/
int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}
	return 0;
}