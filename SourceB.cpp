#include "sqlite3.h"
#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::string;

bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg);
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg);
int callback(void* notUsed, int argc, char** argv, char** azCol);

string result;

int main()
{
	int rc;
	sqlite3* db;
	char* error = 0;
	rc = sqlite3_open("carsDealer.db", &db);
	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return 1;
	}
	carPurchase(12, 21, db, error); //success
	carPurchase(7, 21, db, error); // fail
	carPurchase(3, 22, db, error); //success
	balanceTransfer(3, 4, 1000, db, error);
	system("PAUSE");
	return 0;
}
/*
this function creates a car purchase in the data base
input: the buyer id the car id the data base and error
output: if the transfer success true else false
*/
bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	int rc;
	string command;
	int buyerBalance = 0;
	int carPrice = 0;
	bool available = false;
	//get the balance of the buyer
	command = "select balance from accounts where id=" + std::to_string(buyerid) + ";";
	rc = sqlite3_exec(db, command.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}
	buyerBalance = stoi(result);
	//get the car price
	command = "select price from cars where id=" + std::to_string(carid) + ";";
	rc = sqlite3_exec(db, command.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}
	carPrice = stoi(result);
	//get the car available
	command = "select available from cars where id=" + std::to_string(carid) + ";";
	rc = sqlite3_exec(db, command.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}
	if (result == "1")
	{
		available = true;
	}
	if (available && buyerBalance > carPrice) // if the buyer has enough money and the car is available
	{
		command = "update accounts set balance = "  + std::to_string(buyerBalance - carPrice) + " where id = " + std::to_string(buyerid) + ";";
		rc = sqlite3_exec(db, command.c_str(), NULL, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return false;
		}
		command = "update cars set available = 0 where id = " + std::to_string(carid) + ";";
		rc = sqlite3_exec(db, command.c_str(), NULL, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{ 
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return false;
		}
		return true;
	}
	return false;
}
/*
this function transfer money from one account to second account in the data base
input: the account to take the money from, the account to give the money , the amount of money, the data base and error
output: if the transfer success true else false
*/
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	int rc;
	string command;
	int fromBalance = 0;
	int toBalance = 0;
	command = "select balance from accounts where id=" + std::to_string(from) + ";";
	rc = sqlite3_exec(db, command.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}
	fromBalance = stoi(result);
	command = "select balance from accounts where id=" + std::to_string(to) + ";";
	rc = sqlite3_exec(db, command.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}
	toBalance = stoi(result);
	rc = sqlite3_exec(db, "begin transaction;", NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}
	command = "update accounts set balance = " + std::to_string(fromBalance - amount) + " where id = " + std::to_string(from) + ";";
	rc = sqlite3_exec(db, command.c_str(), NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}
	command = "update accounts set balance = " + std::to_string(toBalance + amount) + " where id = " + std::to_string(to) + ";";
	rc = sqlite3_exec(db, command.c_str(), NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}
	rc = sqlite3_exec(db, "commit;", NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}
	return true;
}
/*
this function return the output from sqlite 3 and print it
*/
int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	for (int i = 0; i < argc; i++)
	{
		cout << azCol[i] << " = " << argv[i] << endl;
	}
	result = argv[0];
	return 0;
}